import './App.css';
import ResetPasswordPage from './components/ResetPasswordPage';
import ImportOrdersPage from './components/ImportOrdersPage';
import TrackPage from './components/TrackPage';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/track/:key">
          <TrackPage />
        </Route>
        <Route path="/resetPassword/:key">
          <ResetPasswordPage />
        </Route>
        <Route exact={true} path="/import">
          <ImportOrdersPage />
        </Route>
        <Route path="/import/:key">
          <ImportOrdersPage />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
