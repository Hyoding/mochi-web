import React, {useState, useCallback} from 'react';
import {useParams} from 'react-router-dom';
import {TextField, Button, makeStyles, Container, Badge, Typography} from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';
import {useDropzone} from 'react-dropzone';
import {PuffLoader} from "react-spinners";

import Constants from '../Constants';
import AppBar from './appBar';
import ReactMapboxGl, { Marker } from 'react-mapbox-gl';
import { Room } from '@material-ui/icons';
import 'mapbox-gl/dist/mapbox-gl.css';
import TimezoneSelect from 'react-timezone-select'

const Map = ReactMapboxGl({
  accessToken: Constants.MapApiKey,
});

const containerStyle = {
  width: '100%',
  height: '60vh'
};

const useStyles = makeStyles(theme => ({
  textField: {
    width: '100%',
    marginTop: 15,
  },
  importOrdersSection: {
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    marginBottom: 10,
  },
  importButton: {
    marginTop: 10,
    width: 200,
  }
}));

export default function ImportOrdersPage() {
  const {key} = useParams();
  const classes = useStyles();

  const [selectedFiles, setSelectedFiles] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const [loadedOrders, setLoadedOrders] = useState([]);
  const [selectedIdx, setSelectedIdx] = useState(null);
  const [businessKey, setBusinessKey] = useState('');
  const [selectedTimezone, setSelectedTimezone] = useState({});

  const onDrop = useCallback((acceptedFiles) => {
    const files = [];
    acceptedFiles.forEach((file) => {
      files.push(file);
    });
    setSelectedFiles(files);
  }, [])
  const {getRootProps, getInputProps} = useDropzone({onDrop});

  const importOrders = () => {
    setLoading(true);
    fetch(Constants.Url.importOrders, {
      method: "POST",
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        businessKey: key ? key : businessKey,
        orders: loadedOrders,
      }),
    })
    .then(async response => {
      const data = await response.json();
      if (!response.ok) {
        const error = (data && data.message) || response.status;
        return Promise.reject(error);
      }
      window.location.reload();
      alert(data.message);
    })
    .catch(err => {
      setLoading(false);
      alert(err);
    });
  };

  const loadFile = () => {
    console.log(selectedTimezone);
    if (selectedFiles.length === 0) {
      return alert('Please select files');
    }
    if (!key && businessKey.length === 0) {
      return alert('Please provide business key');
    }
    if (Object.keys(selectedTimezone).length === 0) {
      return alert('Please select a timezone');
    }
    setLoading(true);
    const data = new FormData();
    data.append("businessKey", key ? key : businessKey);
    data.append("timeZone", selectedTimezone.value);
    selectedFiles.forEach(file => {
      data.append("files", file);
    });
    fetch(Constants.Url.loadOrders, {
      method: "POST",
      body: data,
    })
    .then(async response => {
      const data = await response.json();
      if (!response.ok) {
        const error = (data && data.message) || response.status;
        return Promise.reject(error);
      }
      console.log(data);
      setLoadedOrders(data.responseData);
      setLoading(false);
    })
    .catch(err => {
      alert(err);
      setLoading(false);
    });
  };

  const convertCoordToArray = (coord) => {
    return [
      coord.longitude,
      coord.latitude,
    ];
  }

  const printOrder = (order) => {
    const {address, deliverBy, email, note, phoneNumber, tags} = order;
    const {city, postalCode, province, streetAddress, unit} = address;
    return (
      <>
        <Typography variant="subtitle1">{`${unit ? `#${unit} - ` : ''} ${streetAddress}, ${city}, ${province}, ${postalCode}`}</Typography>
        <Typography variant="subtitle2">{`${phoneNumber}, ${email}`}</Typography>
        <Typography variant="subtitle2">{`Deliver by: ${new Date(deliverBy).toLocaleString()}`}</Typography>
        <Typography variant="subtitle2">{`Note: ${note}`}</Typography>
        <Typography variant="subtitle2">{`Tags: ${tags.join(', ')}`}</Typography>
      </>
    );
  };

  if (true) {
    console.log('key', key);
  }
  
  return (
    <>
      <AppBar />
      {isLoading &&
        <div style={{
          height: '60vh',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
          <PuffLoader
            size={50}
            color={"#123abc"}
            margin={15}
            loading={true}
          />
        </div>
      }
      {loadedOrders.length > 0 && !isLoading &&
        <div className={classes.importOrdersSection}>
          <Map
            style={Constants.MapStyle}
            containerStyle={containerStyle}
            center={selectedIdx ? convertCoordToArray(loadedOrders[selectedIdx].address.coordinate) : convertCoordToArray(loadedOrders[0].address.coordinate)}
          >
            {loadedOrders.map((o, idx) => 
              <Marker 
                key={idx}
                coordinates={convertCoordToArray(o.address.coordinate)}
                onClick={() => setSelectedIdx(idx)}
              >
                <Badge badgeContent={idx+1}>
                  <Room style={{color: 'red'}} />
                </Badge>
              </Marker>)
            }
          </Map>
          {selectedIdx != null &&
            <div style={{width: '100%'}}>
              <Alert severity="info">
                <AlertTitle>{`Order# ${selectedIdx + 1}`}</AlertTitle>
                {printOrder(loadedOrders[selectedIdx])}
              </Alert>
            </div>
          }
          <Typography style={{marginTop: 10}}>{`Loaded count: ${loadedOrders.length}`}</Typography>
          <Button style={{backgroundColor: 'red'}} onClick={importOrders} className={classes.importButton} variant="contained" color="primary">
            <Typography>Import</Typography>
          </Button>
        </div>
      }
      <Container style={{marginTop: 10}}>
        <div style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 20,
          marginBottom: 20,
        }}>
          <Typography>INSTRUCTION: Download and fill out <a href='/importTemplate.xlsx' download>this template</a> to import orders in bulk. Only excel files are accepted. It's important to select correct timezone</Typography>
        </div>
        <div className={classes.importOrdersSection}>
          <div {...getRootProps()} style={{
            width: '80%',
            border: '1px dashed',
            justifyContent: 'center',
            display: 'flex',
            padding: 20,
          }}>
            <input {...getInputProps()} />
            {selectedFiles.length === 0 &&
              <p>Drag and drop files or click</p>
            }
            {selectedFiles.length !== 0 &&
              <p>{selectedFiles.map(f => f.name).join(', ')}</p>
            }
          </div>
          {!key &&
            <TextField 
              style={{width: '80%'}}
              value={businessKey} onChange={(e) => {setBusinessKey(e.target.value)}} className={classes.textField} id="outlined-basic" label="Business key" variant="outlined"
            />
          }
          <blockquote>Select orders timezone</blockquote>
          <div style={{width: '80%'}}>
            <TimezoneSelect
              value={selectedTimezone}
              onChange={setSelectedTimezone}
            />
          </div>
          <Button onClick={loadFile} className={classes.importButton} variant="contained" color="primary">
            <Typography>Load File</Typography>
          </Button>
        </div>
      </Container>
    </>
  );

}
