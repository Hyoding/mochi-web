import React from 'react';
import { ListSubheader, List, ListItem, Divider, Modal, makeStyles, Button, Icon, Container } from '@material-ui/core';
import { Link } from "react-router-dom";

import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[10],
  },
  modalContent: {
    paddingLeft: 20,
    paddingRight: 20,
  },
  modalButton: {
    width: '100%',
  },
  modalItemCounter: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20,
  },
  itemCount: {
    marginLeft: 10,
    marginRight: 10,
  },
  cartItem: {
    display: 'flex',
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-evenly',
  },
  cartItemColumn: {
    flex: 1,
  },
}));

const items = [
  {
    key: 'Hamburger',
    name: '햄버거',
    description: 'Beef patty, onions, tomatoes, pickles, lettuce, ketchup, mayonnaise, and mustard.',
    price: '7.99'
  },
  {
    key: 'Hot Dog',
    name: 'Hot Dog',
    description: 'Juicy one',
    price: '3.99'
  },
  {
    key: 'pop',
    name: 'Pop',
    description: null,
    price: '1.50'
  },
];

function ItemCount({item, count}) {
  this.item = item;
  this.count = count;
}

function Order() {
  this.items = [];
  this.specialRequest = null;
  
  this.calculateSubtotal = function() {
    var subtotal = 0;
    this.items.forEach(i => subtotal += i.item.price * i.count);
    return subtotal;
  }

}

export default function BusinessPage() {
  const classes = useStyles();
  const [selectedItem, selectItem] = React.useState(null);
  const [itemCount, setItemCount] = React.useState(1);
  const [order, setOrder] = React.useState(new Order());
  const [showCheckout, setShowCheckout] = React.useState(false);

  const icrementCounter = () => {
    setItemCount(itemCount + 1);
  };
  
  const decrementCounter = () => {
    if (itemCount === 1) {
      return;
    }
    setItemCount(itemCount - 1);
  };

  const closeModal = () => {
    selectItem(null);
    setItemCount(1);
  };

  const addToCart = () => {
    const item = order.items.find(i => i.item.key === selectedItem.key);
    if (item) {
      item.count += itemCount;
    } else {
      order.items.push(new ItemCount({item: selectedItem, count: itemCount}));
    }
    setOrder(order);
    closeModal();
    console.log(order);
  };

  const removeFromCart = (itemKey) => {
    console.log(itemKey);
    const updatedOrder = new Order();
    updatedOrder.items = order.items.filter(i => i.item.key !== itemKey);
    updatedOrder.specialRequest = order.specialRequest;
    setOrder(updatedOrder);
  };

  return (
    <div>
      <Typography variant="h3">Chef Jong's Food Truck</Typography>
      <List subheader={<ListSubheader>Items</ListSubheader>} >
        {
          items.map((i, key) => (
            <div key={key}>
              {(key > 0) && <Divider />}
              <ListItem button onClick={() => selectItem(i)}>
                <div>
                  <Typography variant="h5">{i.name}</Typography>
                  <Typography variant="subtitle2">{i.description}</Typography>
                  <Typography>{`$${i.price}`}</Typography>
                </div>
              </ListItem>
            </div>
          ))
        }
      </List>
      <div>
        <Typography variant="h5">Cart</Typography>
        <Divider />
        <List>
          {order.items.map((i, key) => (
            <div key={key}>
              <ListItem>
                <div className={classes.cartItem}>
                  <Typography className={classes.cartItemColumn}>{i.count}</Typography>
                  <Typography className={classes.cartItemColumn}>{i.item.name}</Typography>
                  <Typography className={classes.cartItemColumn}>{`$${i.item.price * i.count}`}</Typography>
                  <Icon color="secondary" onClick={() => removeFromCart(i.item.key)}>remove_circle</Icon>
                </div>
              </ListItem>
              <Divider />
            </div>
          ))}
        </List>
        <Typography>{`Subtotal: $${order.calculateSubtotal()}`}</Typography>
        <Button variant="contained" color="primary" onClick={() => setShowCheckout(true)}>Check out</Button>
      </div>

      {/* item select modal */}
      <Modal
        disableAutoFocus
        disableEnforceFocus
        aria-labelledby="item-select"
        aria-describedby="select item"
        className={classes.modal}
        open={selectedItem != null}
        onClose={closeModal}
      >
        <div className={classes.paper}>
          <div className={classes.modalContent}>
            <h2 id="simple-modal-title">{selectedItem && selectedItem.name}</h2>
            <p id="simple-modal-description">{selectedItem && selectedItem.description}</p>
          </div>
          <div className={classes.modalItemCounter}>
            <Icon onClick={decrementCounter}>remove_circle</Icon>
            <Typography className={classes.itemCount}>{itemCount}</Typography>
            <Icon onClick={icrementCounter}>add_circle</Icon>
          </div>
          <Button className={classes.modalButton} variant="contained" color="primary" onClick={addToCart}>
            Add to cart
          </Button>
        </div>
      </Modal>

      {/* checkout modal */}
      <Modal
        disableAutoFocus
        disableEnforceFocus
        aria-labelledby="checkout"
        aria-describedby="checkout"
        className={classes.modal}
        open={showCheckout}
        onClose={() => setShowCheckout(false)}
      >
        <div className={classes.paper}>
          <div className={classes.modalContent}>
            <h2 id="simple-modal-title">{'Checkout'}</h2>
            <p id="simple-modal-description">{selectedItem && selectedItem.description}</p>
          </div>
          <Container>
            <span>Special Request?</span><br />
            <span>Will not accept after this time?</span><br />
            <span>tip?</span>
          </Container>
          <Link to="track">
            <Button className={classes.modalButton} variant="contained" color="primary">
              Place Order
            </Button>
          </Link>
        </div>
      </Modal>
    </div>
  );
}
