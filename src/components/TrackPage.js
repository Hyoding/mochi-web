import React, {useEffect} from 'react';
import { Typography, Container } from '@material-ui/core';
import {useParams} from 'react-router-dom';

import ReactMapboxGl, { Marker } from 'react-mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import Constants from '../Constants';
import {PuffLoader} from "react-spinners";
import { Home, BusinessOutlined, LocalShipping } from '@material-ui/icons';
import AppBar from './appBar';

const center = [];

const Map = ReactMapboxGl({
  accessToken: Constants.MapApiKey,
});

const containerStyle = {
  width: '100%',
  height: '55vh'
};

export default function TrackPage() {
  const {key} = useParams();
  const [state, setState] = React.useState(null);

  useEffect(() => {
    trackOrder();
    const interval = setInterval(trackOrder, 10000);
    return () => clearInterval(interval);
  }, []);

  const trackOrder = () => {
    fetch(`${Constants.Url.trackOrder}?orderKey=${key}`)
    .then(response => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error('bad');
      }
    })
    .then(data => {
      Constants.AppTitle = data.responseData.business;
      console.log('data', data);
      if (center.length === 0) {
        center.push(data.responseData.order.address.coordinate.longitude);
        center.push(data.responseData.order.address.coordinate.latitude);
      }
      setState(data.responseData);
    })
    .catch(ex => {
      console.log(ex);
    });
  };

  const convertCoordToArray= (coord) => {
    return [
      coord.longitude,
      coord.latitude,
    ];
  }

  if (!state) {
    return (
      <div style={{
        height: '100vh',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      }}>
        <PuffLoader
          size={50}
          color={"#123abc"}
          margin={15}
          loading={true}
        />
      </div>
    );
  } else {
    var outForDelivery = state.order.status === 'Out for delivery';
    return (
      <>
        <AppBar />
        {state &&
          <Map
            style={Constants.MapStyle}
            containerStyle={containerStyle}
            center={center}
          >
            <Marker
              coordinates={convertCoordToArray(state.order.address.coordinate)}
              anchor="bottom"
            >
              <Home/>
            </Marker>
            <Marker coordinates={convertCoordToArray(state.businessLocation)} anchor="bottom">
              <BusinessOutlined/>
            </Marker>
            {(state.coordinate && outForDelivery) &&
             <Marker coordinates={convertCoordToArray(state.coordinate)} anchor="bottom">
              <LocalShipping style={{color: 'ef5350'}}/>
            </Marker> 
            }
          </Map>
        }
        <Container maxWidth='xl'>
          <Typography>{`Status: ${state.order.status}`}</Typography>
          <Typography>{`Promised time: ${new Date(state.order.deliverBy).toLocaleString()}`}</Typography>
          {state.tripStartedTime &&
            <Typography>{`Trip started: ${new Date(state.tripStartedTime).toLocaleString()}`}</Typography>
          }
          {state.sequenceNumber &&
            <Typography>{`Delivery sequence: ${state.sequenceNumber}`}</Typography>
          }
          {state.deliveredTime &&
            <Typography>{`Delivered time: ${new Date(state.deliveredTime).toLocaleString()}`}</Typography>  
          }
          {!state.deliveredTime &&
            <Typography>{`Last updated: ${state.lastUpdated == null ? 'N/A' : new Date(state.lastUpdated).toLocaleString()}`}</Typography>
          }
        </Container>
      </>
    )
  }
}
