import React, {useEffect} from 'react';
import {useParams} from 'react-router-dom';
import {TextField, Button, makeStyles, Container} from '@material-ui/core';
import Constants from '../Constants';
import AppBar from './appBar';

const useStyles = makeStyles(theme => ({
  textField: {
    width: '100%',
    marginTop: 15,
  },
  loginButtonSection: {
    marginTop: 10,
    width: '100%',
    height: '30%',
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
  },
  loginButton: {
    width: 200,
  }
}));

export default function TrackPage() {
  const classes = useStyles();
  const {key} = useParams();
  const [state, setState] = React.useState({
    pwd1: '',
    pwd2: '',
  });
 
  useEffect(() => {

  });

  const resetPwd = () => {
    const {pwd1,pwd2} = state;
    console.log('hey', pwd1, pwd2);
    if (pwd1 === '') {
      alert('Put in both passwords');
      return;
    }
    if (pwd1 !== pwd2) {
      alert('Passwords are not matching');
      return;
    }
    

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ key, password: pwd1 })
    };
    fetch(Constants.Url.resetEmpPwd, requestOptions)
    .then(async response => {
      const data = await response.json();
      if (!response.ok) {
          const error = (data && data.message) || response.status;
          return Promise.reject(error);
      }
      alert(data.message);
      window.location.reload();
    })
    .catch(err => {
      alert(err);
    });
  };
 
  return (
    <>
      <AppBar />
      <Container>
        <div>
          <TextField 
            type="password"
            autoComplete="current-password"
            value={state.pwd1} onChange={(e) => {setState({pwd1:e.target.value, pwd2: state.pwd2})}} className={classes.textField} id="outlined-basic" label="new password" variant="outlined" />
          <TextField 
            type="password"
            autoComplete="current-password"
            value={state.pwd2} onChange={(e) => {setState({pwd2:e.target.value, pwd1: state.pwd1})}} className={classes.textField} id="outlined-basic" label="confirm password" variant="outlined" />
        </div>
        <div className={classes.loginButtonSection}>
          <Button onClick={resetPwd} className={classes.loginButton} variant="contained" color="primary">
            Reset
          </Button>
        </div>
      </Container>
    </>
  );

}
