// const BASE_URL = 'http://192.168.0.103:61611';
const BASE_URL = 'http://68.183.127.26:61611';

const Constants = {
  MapApiKey: 'pk.eyJ1Ijoiam9zZXBoLXl1IiwiYSI6ImNrZ29mMTI5YjBmNmYycm5xczlodGY1N3AifQ.vQwCHPb3G9C7QPjVrrgG4Q',
  MapStyle: 'mapbox://styles/joseph-yu/ckhtm7t9u359n19nydml1ohv4',
  Url: {
    trackOrder: `${BASE_URL}/customer/v1/trackOrder`,
    resetEmpPwd: `${BASE_URL}/business/v1/resetEmpPwd`,
    loadOrders: `${BASE_URL}/api/business/v1/loadOrdersFile`,
    importOrders: `${BASE_URL}/api/business/v1/importOrders`,
  },
  AppTitle: 'Mochi',
};

export default Constants;
